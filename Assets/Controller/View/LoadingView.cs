﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingView : MonoBehaviour
{
    [SerializeField]
    private float delayBeforeLoading = 3f;

    private float timeElapsed;

    private void Update()
    {
        timeElapsed += Time.deltaTime;

        if (timeElapsed > delayBeforeLoading)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}
